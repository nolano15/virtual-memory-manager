import java.io.BufferedInputStream;
import java.util.*;
import java.util.function.Predicate;

/**
 * @author Nolan O'Shea
 * Class: CS 4337
 * Assignment: Prog3
 */
public class MemoryManager {
	// Memory to Manage
	List<MemSlice> mem;
	
	public static void main(String[] args) {
		new MemoryManager(Integer.parseInt(args[0])).go();
	}
	
	/**
	 * @param bytes user input size of memory
	 */
	MemoryManager(int bytes) {
		if(bytes <= 0) {
			System.out.println("Specify greater than zero bytes");
			System.exit(1);
		}
		
		mem = new ArrayList<MemSlice>(1);
		mem.add(getInitialMemSlice(bytes));
	}
	
	private void go() {
		try(var userIn = new Scanner(new BufferedInputStream(System.in))) {
			final String ADD_CMD = "a(dd) <id> <size> <method>",
						 FREE_CMD = "f(ree) <item>",
						 METHODS = "f(irst fit), b(est fit), or w(orst fit)";
			
			while(true) {
				// User input
				String[] cmd_args = userIn.nextLine().split(" ");
				
				if(cmd_args.length == 0) {
					continue; // ignore empty inputs
				}
				
				// switch to proper command case
				switch(cmd_args[0]) {
				case "a":
					if(cmd_args.length != 4) {
						System.out.println("Usage: " + ADD_CMD);
						break;
					}
					try {
						int id = Integer.parseInt(cmd_args[1]),
							size = Integer.parseInt(cmd_args[2]);
						
						if(size <= 0) {
							System.out.println("Size must be greater than zero");
							break;
						}
						
						FitStrategy fitStrategy = null;
						String method = cmd_args[3];
						
						switch(method) {
						case "f": fitStrategy = new FirstFit(); break;
						case "b": fitStrategy = new BestFit(); break;
						case "w": fitStrategy = new WorstFit();
						}
					
						add(id, size, Objects.requireNonNull(fitStrategy, "Unknown method"));
					} catch(NumberFormatException e) {
						System.out.println("Invalid ID/size");
					} catch(NullPointerException e) {
						System.out.println(e.getMessage());
						System.out.println(METHODS);
						// item was not added
					}
					break;
				case "f": 
					if(cmd_args.length != 2) {
						System.out.println("Usage: " + FREE_CMD);
						break;
					}
					try {
						int id = Integer.parseInt(cmd_args[1]);
						free(id);
					} catch(NumberFormatException e) {
						System.out.println(e.getMessage());
					}
					break;
				case "d": display(); break;
				case "q": System.exit(1);
				default: System.out.println("Unknown command\nValid commands: " +
						ADD_CMD + ", " + FREE_CMD + ", d(isplay), q(uit)");
				}
			}
		}
	}
	
	/**
	 * Adds an item to memory, guaranteeing unique IDs
	 * 
	 * @param id id of item to be added
	 * @param size byte size of item to be added
	 * @param method specific Fitting algorithm to be applied
	 */
	void add(int id, int size, FitStrategy method) {
		if(filterID(id).isPresent()) {
			System.out.println("Item with that ID already in memory");
			// item will not be added
		} else {
			method.fit(getMemSliceBeforeFit(id, size));
		}
	}
	
	/**
	 * Frees a MemSlice if matching id is found
	 * 
	 * @param id item id to free
	 */
	void free(int id) {
		Optional<MemSlice> toFree = filterID(id);
		
		if(!toFree.isPresent()) {
			System.out.println("Failed! Item was not in memory");
		} else {
			toFree.get().free = true;
			
			System.out.println("Success! The block is freed");
			
			coalesce();
		}
	}
	
	/**
	 * Coalesces adjacent free blocks if possible
	 */
	void coalesce() {
		for(int i = 1; i < mem.size(); i++) {
			MemSlice msi = mem.get(i - 1),
					 msj = mem.get(i);
			
			// if these two MemSlices are both free
			if(msi.free && msj.free) {
				// then coalesce
				// only need to combine their sizes
				// removes a slice at the same time
				// can continue looping
				// i is post decremented to repeat iteration
				msi.size += mem.remove(i--).size;
			}
		}
	}
	
	/**
	 * Helper function that filters all MemSlices through a test ID
	 * 
	 * @param id test id
	 * 
	 * @return matching MemSlice, if any
	 */
	Optional<MemSlice> filterID(int id) {
		return mem.parallelStream().filter(ms -> ms.idMatch(id)).findAny();
	}
	
	/**
	 * Display each MemSlice to the console
	 */
	void display() {
		mem.forEach(System.out::println);
	}
	
	abstract class FitStrategy {
		// Fields will have inverted values for Best/Worst Fit
		int snugness;
		Predicate<Integer> bw; // Strategy Design Pattern
		
		public void fit(MemSlice new_item) {
			MemSlice selected_block = null;
			
			for(int i = 0; i < mem.size(); i++) {
				MemSlice msi = mem.get(i);
				
				if(new_item.canFitIn(msi)) {
					int diff = msi.size - new_item.size;
					
					// custom test
					if(bw.test(diff)) {
						snugness = diff;
						// save reference
						selected_block = msi;
						
						// FirstFit does not scan entire List
						if(this instanceof FirstFit) {
							break;
						}
					}
				}
			}
			replace(new_item, selected_block);
		}
	}
	
	class FirstFit extends FitStrategy {
		FirstFit() {
			bw = i -> true; // any Fit is good enough
		}
	}
	
	class BestFit extends FitStrategy {
		BestFit() {
			snugness = Integer.MAX_VALUE;
			bw = i -> snugness > i;
		}
	}
	
	class WorstFit extends FitStrategy {
		WorstFit() {
			snugness = Integer.MIN_VALUE;
			bw = i -> snugness < i;
		}
	}
	
	/**
	 * Inserts a new item into a selected free block of memory
	 * 
	 * @param new_item Item being added
	 * @param free_block Block of memory that FitStrategy has selected
	 */
	void replace(MemSlice new_item, MemSlice free_block) {
		try {
			// save index before removing, if item can be added, else throw Exception
			int i = mem.indexOf(Objects.requireNonNull(free_block, "Item could not be added"));
			
			mem.remove(free_block);			
			mem.add(i, new_item);
			
			System.out.println("Item successfully added!");
			
			// calculate remaining free bytes
			free_block.size -= new_item.size;
			
			// if there are still free bytes, add them back
			if(free_block.size > 0) {
				mem.add(i + 1, free_block);
			}
		} catch(NullPointerException e) {
			System.out.println(e.getMessage());
			System.out.println("Inadequate memory space available. Free an item and try again");
		}
	}
	
	/**
	 * Factory Method that creates a MemSlice before FitStrategy is applied
	 * 
	 * @param id id of new item being added
	 * @param size byte size of item being added
	 * 
	 * @return MemSlice that is ready to be passed into FitStrategy 
	 */
	MemSlice getMemSliceBeforeFit(int id, int size) {
		return new MemSlice().setId(id).setSize(size).setFree(false);
	}
	
	/**
	 * Factory method that returns the initial MemSlice
	 * 
	 * @param size byte size of initial MemSlice
	 * 
	 * @return initial MemSlice
	 */
	MemSlice getInitialMemSlice(int size) {
		return new MemSlice().setSize(size).setFree(true);
	}
	
	// Struct representing a block in memory
	class MemSlice {
		int size, id; // ID is meaningless in free blocks
		boolean free;
		
		// fluid interface
		public MemSlice setSize(int size) {
			this.size = size;
			return this;
		}

		public MemSlice setId(int id) {
			this.id = id;
			return this;
		}

		public MemSlice setFree(boolean free) {
			this.free = free;
			return this;
		}
		
		/**
		 * Calculates which byte this MemSlice starts from
		 * 
		 * @return start byte
		 */
		int getStart() {
			int start = 0;
			
			for(MemSlice ms : mem) {
				// break loop when this is reached
				if(this.equals(ms)) {
					return start;
				} else {
					start += ms.size;
				}
			}	
			return -1; // should never be reached
		}
		
		/**
		 * Tests whether input MemSlice is available and can Fit this
		 * 
		 * @param ms candidate MemSlice
		 * 
		 * @return true if this MemSlice can be inserted into ms
		 */
		boolean canFitIn(MemSlice ms) {
			return ms.free && ms.size >= size;
		}

		/**
		 * Tests whether this MemSlice matches an id
		 * Only valid to test if this Memslice is not free, and therefore contains a valid id
		 * 
		 * @param id test id
		 * 
		 * @return true if this MemSlice matches test id
		 */
		boolean idMatch(int id) {
			return !free && this.id == id;
		}

		@Override
		public String toString() {
			int start = getStart();
			
			return (start + 1) +
					// if Slice is a single byte
					(size > 1 ? "-" + (start + size) : "") + "\t" +
					(free ? "Free" : "Item " + id);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + (free ? 1231 : 1237);
			result = prime * result + id;
			result = prime * result + size;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MemSlice other = (MemSlice) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (free != other.free)
				return false;
			if (id != other.id)
				return false;
			if (size != other.size)
				return false;
			return true;
		}

		private MemoryManager getOuterType() {
			return MemoryManager.this;
		}
	}
}
